package com.kruno.teltechmvvm

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.kruno.teltechmvvm.data.api.SpaceShipService
import com.kruno.teltechmvvm.data.models.SpaceShipDto
import com.kruno.teltechmvvm.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.InputStreamReader

@RunWith(AndroidJUnit4::class)
class SpaceShipApiTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    lateinit var mockServer: MockWebServer
    lateinit var spaceShipService: SpaceShipService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        mockServer = MockWebServer()
        mockServer.start(8080)
        val okHttpClient = OkHttpClient.Builder().build()
        val retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constants.BASE_URL)
            .client(okHttpClient).build()
        spaceShipService = retrofit.create(SpaceShipService::class.java)
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun webApiResponseTest() {
        var ctx: Context = InstrumentationRegistry.getTargetContext()
        var inputStream = ctx.classLoader.getResourceAsStream("assets" + File.separator + "swapi.json")
        var jsonElement: JsonElement = JsonParser().parse(InputStreamReader(inputStream))
        inputStream.close()
        var gson = Gson()
        val spaceShipDto: SpaceShipDto = gson.fromJson(jsonElement, SpaceShipDto::class.java)

        var apiResponse = spaceShipService.getStarShipList(4).execute()
        Assert.assertEquals(apiResponse.body()?.result?.get(0), spaceShipDto.result.get(0))
    }

    @Test
    fun mockApiResponseTest() {

    }
}