package com.kruno.teltechmvvm

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.kruno.teltechmvvm.data.api.SpaceShipService
import com.kruno.teltechmvvm.data.database.LocalDatabase
import com.kruno.teltechmvvm.data.models.SpaceShipModel
import com.kruno.teltechmvvm.data.repository.SpaceShipRepository
import com.kruno.teltechmvvm.utils.Constants
import com.kruno.teltechmvvm.viewmodel.SpaceShipViewModel
import okhttp3.OkHttpClient
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

@RunWith(AndroidJUnit4::class)
class SpaceShipViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule() as TestRule

    lateinit var database: LocalDatabase
    lateinit var spaceshipViewModel: SpaceShipViewModel
    lateinit var spaceShipService: SpaceShipService

    val spaceShipName: String = "Executor"

    var spaceShipModel: SpaceShipModel = SpaceShipModel(
        spaceShipName, "", "",
        "", "", "", "", "", "",
        "", "", "", ""
    )

    @Mock
    var listObserver: Observer<MutableList<SpaceShipModel>>? = null

    @Mock
    var objectObserver: Observer<SpaceShipModel>? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        var context: Context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, LocalDatabase::class.java).allowMainThreadQueries().build()

        val okHttpClient = OkHttpClient.Builder().build()
        val retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constants.BASE_URL)
            .client(okHttpClient).build()
        spaceShipService = retrofit.create(SpaceShipService::class.java)
        spaceshipViewModel = SpaceShipViewModel(SpaceShipRepository(database, spaceShipService))
    }

    @After
    fun tearDown() {
        database?.close()
    }

    @Test
    fun getSpaceshipListAndVerifyObserverChange() {
        spaceshipViewModel.getSpaceShipList()?.observeForever(listObserver as Observer<MutableList<SpaceShipModel>>)
        database.spaceShipDao().insertSpaceShip(spaceShipModel)

        Mockito.verify(listObserver)?.onChanged(Collections.singletonList(spaceShipModel))
    }

    @Test
    fun getSpaceShipByNameAndVerifyQuery() {
        database.spaceShipDao().insertSpaceShip(spaceShipModel)
        spaceshipViewModel.getSpaceShipByName(spaceShipName)?.observeForever(objectObserver as Observer<SpaceShipModel>)

        Mockito.verify(objectObserver)?.onChanged(spaceShipModel)
    }
}