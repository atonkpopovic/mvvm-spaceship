package com.kruno.teltechmvvm.data.models

import com.google.gson.annotations.SerializedName

data class SpaceShipDto(
    @SerializedName("count") val count: Int,
    @SerializedName("next") val next: String,
    @SerializedName("results") val result: List<SpaceShipModel>
)