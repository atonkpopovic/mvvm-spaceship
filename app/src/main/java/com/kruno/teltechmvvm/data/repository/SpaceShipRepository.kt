package com.kruno.teltechmvvm.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.support.annotation.WorkerThread
import android.util.Log
import android.view.animation.Transformation
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kruno.teltechmvvm.data.api.SpaceShipService
import com.kruno.teltechmvvm.data.dao.SpaceShipDao
import com.kruno.teltechmvvm.data.database.LocalDatabase
import com.kruno.teltechmvvm.data.models.SpaceShipDto
import com.kruno.teltechmvvm.data.models.SpaceShipModel
import com.kruno.teltechmvvm.utils.Constants
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SpaceShipRepository(private val localDatabase: LocalDatabase, private val spaceShipService: SpaceShipService) {

    val TAG: String = "REPOSITORY"

    fun getSpaceshipList(): LiveData<MutableList<SpaceShipModel>>? {
        return localDatabase.spaceShipDao().getAllSpaceShips()
    }

    fun getSpaceship(name: String): LiveData<SpaceShipModel>? {
        return localDatabase.spaceShipDao().getSpaceShip(name)
    }

    fun insertSpaceship(spaceShip: SpaceShipModel) {
        doAsync { localDatabase.spaceShipDao().insertSpaceShip(spaceShip) }
    }

    fun fetchSpaceShipFromWebService(pageNumber: Int, loading: MutableLiveData<Boolean>) {
        loading.value = true

        spaceShipService.getStarShipList(pageNumber).enqueue(object : Callback<SpaceShipDto?> {
            override fun onFailure(call: Call<SpaceShipDto?>, t: Throwable) {
                Log.d(TAG, "RETROFIT FAILED- ${t.message.toString()}")
                loading.value = false
            }

            override fun onResponse(call: Call<SpaceShipDto?>, response: Response<SpaceShipDto?>) {
                Log.d(TAG, "RETROFIT- ${response.body()?.result.toString()}")
                loading.value = false

                var spaceships: List<SpaceShipModel>? = response.body()?.result
                Log.d(TAG, "RETROFIT TOTAL COUNT ${response.body()?.count}")
                Log.d(TAG, "RETROFIT LIST SIZE ${spaceships?.size}")
                for (spaceShip in spaceships!!) {
                    insertSpaceship(spaceShip)
                    Log.d(TAG, "DATABASE INSERT- ${spaceShip}")
                }

                if (!response.body()?.next.isNullOrEmpty()) {
                    fetchSpaceShipFromWebService(pageNumber + 1, loading)
                }
            }
        })
    }
}