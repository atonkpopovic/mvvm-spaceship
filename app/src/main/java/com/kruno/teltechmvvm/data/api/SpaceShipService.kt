package com.kruno.teltechmvvm.data.api

import com.kruno.teltechmvvm.data.models.SpaceShipDto
import com.kruno.teltechmvvm.utils.Constants
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface SpaceShipService {
    @GET("starships/")
    fun getStarShipList(@Query("page")pageNumber: Int): Call<SpaceShipDto>

    companion object {
        fun create(): SpaceShipService{
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(SpaceShipService::class.java)
        }
    }
}