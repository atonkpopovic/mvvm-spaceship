package com.kruno.teltechmvvm.data.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.kruno.teltechmvvm.data.models.SpaceShipModel

@Dao
interface SpaceShipDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertSpaceShip(spaceship: SpaceShipModel): Long

    @Query("SELECT * FROM SPACE_SHIPS")
    fun getAllSpaceShips(): LiveData<MutableList<SpaceShipModel>>

    @Query("SELECT * FROM SPACE_SHIPS WHERE name= :name ")
    fun getSpaceShip(name: String): LiveData<SpaceShipModel>
}