package com.kruno.teltechmvvm.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "space_ships", indices = arrayOf(Index(value = ["name"], unique = true)))
data class SpaceShipModel(

    @SerializedName("name") var name: String,
    @SerializedName("model") var model: String,
    @SerializedName("manufacturer") var manufacturer: String,
    @SerializedName("cost_in_credits") var costInCredits: String,
    @SerializedName("length") var length: String,
    @SerializedName("max_atmosphering_speed") var maxAtmospheringSpeed: String,
    @SerializedName("crew") var crew: String,
    @SerializedName("passengers") var passengers: String,
    @SerializedName("cargo_capacity") var cargoCapacity: String,
    @SerializedName("hyperdrive_rating") var hyperdriveRating: String,
    @SerializedName("MGLT") var mglt: String,
    @SerializedName("starship_class") var starshipClass: String,
    @SerializedName("consumables") var consumables: String
) {

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}