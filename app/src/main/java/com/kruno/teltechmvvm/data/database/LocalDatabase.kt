package com.kruno.teltechmvvm.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.kruno.teltechmvvm.application.AppController
import com.kruno.teltechmvvm.data.dao.SpaceShipDao
import com.kruno.teltechmvvm.data.models.SpaceShipModel

@Database(entities = [SpaceShipModel::class], version = LocalDatabase.VERSION)
abstract class LocalDatabase : RoomDatabase() {

    abstract fun spaceShipDao(): SpaceShipDao

    companion object {
        const val DB_NAME = "space_ship.db"
        const val VERSION = 1
        private val instance: LocalDatabase by lazy { create(AppController.instance) }

        private fun create(context: Context): LocalDatabase {
            return Room.databaseBuilder(context, LocalDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }

        @Synchronized
        internal fun getInstance(): LocalDatabase {
            return instance
        }
    }
}