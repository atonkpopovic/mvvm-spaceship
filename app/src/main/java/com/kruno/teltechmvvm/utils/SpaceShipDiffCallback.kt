package com.kruno.teltechmvvm.utils

import android.support.v7.util.DiffUtil
import com.kruno.teltechmvvm.data.models.SpaceShipModel

class SpaceShipDiffCallback(private val oldList: List<SpaceShipModel>, private val newList: List<SpaceShipModel>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
        return oldList[p0].id == newList[p1].id
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
        return oldList[p0].name.equals(newList[p1].name)
    }
}