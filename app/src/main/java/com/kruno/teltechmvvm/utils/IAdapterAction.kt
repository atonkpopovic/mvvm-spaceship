package com.kruno.teltechmvvm.utils

interface IAdapterAction {

    fun onItemClick(name: String?)
}