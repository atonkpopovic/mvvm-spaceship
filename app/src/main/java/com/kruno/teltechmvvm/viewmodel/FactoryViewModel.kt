package com.kruno.teltechmvvm.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.kruno.teltechmvvm.data.repository.SpaceShipRepository

class FactoryViewModel(private val repository: SpaceShipRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(SpaceShipViewModel::class.java!!)) {
            SpaceShipViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("Viewmodel not Found")
        }
    }
}