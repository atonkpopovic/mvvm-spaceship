package com.kruno.teltechmvvm.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.kruno.teltechmvvm.data.models.SpaceShipModel
import com.kruno.teltechmvvm.data.repository.SpaceShipRepository

class SpaceShipViewModel(private val repository: SpaceShipRepository) : ViewModel() {

    private var spaceShipList: LiveData<MutableList<SpaceShipModel>>? = null
    private var spaceShip: LiveData<SpaceShipModel>? = null
    private var loading: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    init {
        updateSpaceshipList()
    }

    fun isLoading(): LiveData<Boolean> {
        return loading
    }

    fun getSpaceShipList(): LiveData<MutableList<SpaceShipModel>>? {
        spaceShipList = repository.getSpaceshipList()
        return spaceShipList
    }

    fun getSpaceShipByName(name: String): LiveData<SpaceShipModel>? {
        spaceShip = repository.getSpaceship(name)
        return spaceShip
    }

    fun updateSpaceshipList() {
        repository.fetchSpaceShipFromWebService(1, loading)
    }
}