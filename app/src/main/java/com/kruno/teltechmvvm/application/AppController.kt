package com.kruno.teltechmvvm.application

import android.app.Application
import kotlin.properties.Delegates

class AppController : Application(){

    companion object {
        var instance: AppController by Delegates.notNull()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}