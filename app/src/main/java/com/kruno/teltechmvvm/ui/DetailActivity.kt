package com.kruno.teltechmvvm.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.kruno.teltechmvvm.R
import com.kruno.teltechmvvm.data.api.SpaceShipService
import com.kruno.teltechmvvm.data.database.LocalDatabase
import com.kruno.teltechmvvm.data.repository.SpaceShipRepository
import com.kruno.teltechmvvm.utils.Constants
import com.kruno.teltechmvvm.viewmodel.FactoryViewModel
import com.kruno.teltechmvvm.viewmodel.SpaceShipViewModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    var spaceShipViewModel: SpaceShipViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        var spaceShipName = intent.extras.getString(Constants.SPACESHIP_NAME)
        if (spaceShipName.isNullOrEmpty())
            return

        this.initViewModel()
        this.initUi(spaceShipName)
    }

    fun initViewModel() {
        spaceShipViewModel = ViewModelProviders.of(
            this,
            FactoryViewModel(SpaceShipRepository(LocalDatabase.getInstance(), SpaceShipService.create()))
        )
            .get(SpaceShipViewModel::class.java)
    }

    fun initUi(name: String) {
        spaceShipViewModel?.getSpaceShipByName(name)?.observe(this, Observer { data ->
            supportActionBar?.title = data?.name
            tvStarshipClass.text = "Class: ${data?.starshipClass}"
            tvModel.text = "Model: ${data?.model}"
            tvManufacturer.text = "Manufacturer: ${data?.manufacturer}"
            tvLength.text = "Length: ${data?.length}"
            tvSpeed.text = "Max athmosphering speed: ${data?.maxAtmospheringSpeed}"
            tvCrew.text = "Crew: ${data?.crew}"
            tvPassengers.text = "Passengers: ${data?.passengers}"
            tvHyperdriveRating.text = "Hyper drive rating: ${data?.hyperdriveRating}"
            tvCargoCapacity.text = "Cargo capacity: ${data?.cargoCapacity}"
            tvcCstInCredits.text = "Cost in credits: ${data?.costInCredits}"
        })
    }
}
