package com.kruno.teltechmvvm.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import com.kruno.teltechmvvm.R
import com.kruno.teltechmvvm.data.api.SpaceShipService
import com.kruno.teltechmvvm.data.database.LocalDatabase
import com.kruno.teltechmvvm.data.repository.SpaceShipRepository
import com.kruno.teltechmvvm.ui.adapter.SpaceShipAdapter
import com.kruno.teltechmvvm.utils.Constants
import com.kruno.teltechmvvm.utils.IAdapterAction
import com.kruno.teltechmvvm.viewmodel.FactoryViewModel
import com.kruno.teltechmvvm.viewmodel.SpaceShipViewModel
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v4.content.ContextCompat

class MainActivity : AppCompatActivity(), IAdapterAction {

    private var adapter: SpaceShipAdapter? = null
    private var spaceShipViewModel: SpaceShipViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.initViewModel()
        this.initUi()
    }

    private fun initViewModel() {
        spaceShipViewModel = ViewModelProviders.of(
            this,
            FactoryViewModel(SpaceShipRepository(LocalDatabase.getInstance(), SpaceShipService.create()))
        )
            .get(SpaceShipViewModel::class.java)
    }

    private fun initUi() {
        adapter = SpaceShipAdapter(this)
        recStarShips.layoutManager = LinearLayoutManager(this)
        recStarShips.adapter = adapter
        val mDivider = ContextCompat.getDrawable(this, R.drawable.divider)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(mDivider!!)
        recStarShips.addItemDecoration(itemDecoration)

        spaceShipViewModel?.getSpaceShipList()?.observe(this, Observer { data ->
            adapter!!.update(data!!)
        })

        spaceShipViewModel?.isLoading()
            ?.observe(this, Observer { data -> if (data!!) progress_circular.show() else progress_circular.hide() })
    }

    override fun onItemClick(name: String?) {
        startActivity(Intent(this, DetailActivity::class.java).putExtra(Constants.SPACESHIP_NAME, name))
    }
}
