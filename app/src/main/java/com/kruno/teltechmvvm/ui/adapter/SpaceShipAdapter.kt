package com.kruno.teltechmvvm.ui.adapter

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.kruno.teltechmvvm.R
import com.kruno.teltechmvvm.data.models.SpaceShipModel
import com.kruno.teltechmvvm.utils.IAdapterAction
import com.kruno.teltechmvvm.utils.SpaceShipDiffCallback
import kotlinx.android.synthetic.main.item_star_ship.view.*

class SpaceShipAdapter(val context: Context) : RecyclerView.Adapter<SpaceShipAdapter.ViewHolder>() {
    private var data: MutableList<SpaceShipModel> = mutableListOf<SpaceShipModel>()
    val mListener: IAdapterAction = context as IAdapterAction

    fun update(data: MutableList<SpaceShipModel>) {
        val diffCallback = SpaceShipDiffCallback(this.data, data)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.data.clear()
        this.data.addAll(data)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_star_ship, p0, false))
    }

    override fun getItemCount(): Int {
        if (data == null)
            return 0
        return data!!.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        var spaceShipModel = data?.get(p1)
        p0.tvName.text = "Name - ${spaceShipModel?.name}"
        p0.tvStarShipClass.text = "Class: ${spaceShipModel?.starshipClass}"
        p0.tvStarShipModel.text = "Model: ${spaceShipModel?.model}"
        p0.llRoot.setOnClickListener { mListener.onItemClick(spaceShipModel?.name) }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val llRoot: LinearLayout = view.llRoot
        val tvName: TextView = view.tvName
        val tvStarShipClass: TextView = view.tvStarShipClass
        val tvStarShipModel: TextView = view.tvStarShipModel
    }
}